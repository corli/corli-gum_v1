# Identifiants et textes annotés


| id | année | nom | prénom | textes annotés | couches | profil | commentaires | autorisation |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| masterLITL_UT2J_2023_D | 2023 | Rutali | Chloé | F_wikiDisc_Feminisme_doc000007.xml | | | | |
| | | | | cmr-88milsms-tei-v1_extrait.xml (texte 1 et 2) | | | | |
| masterLITL_UT2J_2023_E | 2023 | Laurence | Estelle | F_wikiDisc_Feminisme_doc000013.xml | | | | |
| | | | | cmr-88milsms-tei-v1_extrait.xml (texte 5 et 6) | | | | |
| masterLITL_UT2J_2023_G | 2023 | Chamboredon | Gaelle | F_wikiDisc_Feminisme_doc000004.xml | | | | |
| | | | | cmr-88milsms-tei-v1_extrait.xml (texte 3 et 4) | | | | |
| masterLITL_UT2J_2023_H | 2023 | Audoye | Johann | F_wikiDisc_Feminisme_doc000007.xml | | | | |
| | | | | cmr-88milsms-tei-v1_extrait.xml (texte 1 et 2) | | | | |
| masterLITL_UT2J_2023_I | 2023 | Labaigt | Julie | F_wikiDisc_Feminisme_doc000013.xml | | | | |
| | | | | cmr-88milsms-tei-v1_extrait.xml (texte 5 et 6) | | | | |
| masterLITL_UT2J_2023_J | 2023 | Sanchez | Julie | F_wikiDisc_Feminisme_doc000007.xml | | | | |
| | | | | cmr-88milsms-tei-v1_extrait.xml (texte 1 et 2)  | | | | |
| masterLITL_UT2J_2023_M | 2023 | Ortiz | Ricardo | F_wikiDisc_Feminisme_doc000004.xml | | | | |
| | | | | cmr-88milsms-tei-v1_extrait.xml (texte 3 et 4) | | | | |

# Identifiants et textes adjudiqués

| id | année | nom | prénom | textes adjudiqués | couches | profil | commentaires | autorisation |
| :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- | :--- |
| masterLITL_UT2J_2023_D | 2023 | Rutali | Chloé | F_wikiDisc_Feminisme_doc000007.xml | | | | |
| masterLITL_UT2J_2023_E | 2023 | Laurence | Estelle | F_wikiDisc_Feminisme_doc000013.xml | | | | |
| masterLITL_UT2J_2023_G | 2023 | Chamboredon | Gaelle | F_wikiDisc_Feminisme_doc000004.xml | | | | |
| masterLITL_UT2J_2023_H | 2023 | Audoye | Johann | cmr-88milsms-tei-v1_extrait.xml (texte 1 et 2) | | | | |
| masterLITL_UT2J_2023_I | 2023 | Labaigt | Julie | cmr-88milsms-tei-v1_extrait.xml (texte 5 et 6) | | | | |
| masterLITL_UT2J_2023_J | 2023 | Sanchez | Julie | F_wikiDisc_Feminisme_doc000007.xml | | | | |
| masterLITL_UT2J_2023_M | 2023 | Ortiz | Ricardo | cmr-88milsms-tei-v1_extrait.xml (texte 3 et 4) | | | | |
