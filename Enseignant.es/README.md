[[_TOC_]]

### CORLI GUM

- Présentation générale du projet CORLI GUM [sur le Wiki](https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/CORLI-GUM)
- [CORLI GUM pour les chercheureuses](https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/Le-projet-CORLI-GUM#les-enseignantes-dans-corli-gum)
- Le guide d'utilisation d'INCEpTION est disponible [ici](https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/2.-INCEpTION)

### Dossiers
#### Fichiers - Corpus
Les fichiers annotés sont à déposer en respectant les [mots-clés] () - page Wiki à venir.
A chaque fichier déposé, indiquer les mots-clefs et décrire toutes remarques lié au traitement de cette version du fichier sur le [Wiki du corpus](https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/Le-projet-CORLI-GUM/Corpus)

#### INCEpTION - Modèles d'annotation
A déposer dans le dossier dédié

Le guide INCEpTION est disponible sur le [Wiki](https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/Le-projet-CORLI-GUM/INCEpTION)

#### Supports de cours 
A déposer dans le dossier dédié

#### projetsUniversitaires
Pour chaque projet universitaires impliqués dans le projet CORLI-GUM, la/le responsable prédagogique doit créer dans ce dossier un dossier au nom de son projet (formation_université) qui contiendra un sous-dossier par année de participation. Ce sous-dossier sera composé d'un document donnant une description et un bilan des annotations réalisées et un sous-dossier intitulé fichesEtudiants qui contiendra autant de fiches que d'étudiant.e.s impliqué.e.s   





















