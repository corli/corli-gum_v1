Le dossier "fiches" contient les fiches décrivant le profil et les annotations et adjudications réalisées par chaque participant au projet CORLI-GUM.
L'indication des annotations doit être associé à la couche d'annotation concernée en suivant la syntaxe suivante : 
labelCouches_version complète : STDERM (si version incomplète : STDxRx)
- S : phrase (segmentation et type selon le modèle GUM)
- T : token (segmentation et POStagging selon le modèle des Universal Dependencies, lemme et normalisation orthographique si nécessaire)
- D : dépendances syntaxique entre tokens selon le modèle des Universal Dependencies
- E : entités nommées (segmentation des entitées nommées et typage selon le modèle Quaero)
- R : (co)référence (segmentation des mentions selon le modèle DemoCrat, y compris les singletons, et relations de coréférence)
- M : marqueurs discursifs (segmentation des marqueurs discursifs et typage selon le modèle de Crible & Degand (2019)


Pour ajouter une fiche participant, télécharger le fichier ficher_FFFF_YYYY_XX.txt, le compléter avec les informations requises et envoyez la à lydia-mai.ho-dac@univ-tlse2.fr.
Veillez à ce que le nom du fichier soit l'ID du participant avec l'extension .txt
