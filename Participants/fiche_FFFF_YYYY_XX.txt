Ce fichier une fois rempli doit être renommé "fiche_"+$ID où $ID correspond à l'ID attribuée (ex : masterLITL_UT2J_YYYY_LMHD)

ID attribuée : 
NOM : 
Prénom : 
Adresse mail :
Institution (Université, Laboratoire, etc.) : 
Statut (étudiant.e, ingénieur.e, chercheur.e, enseignant.e) : 
Année scolaire & nom du parcours : 
Autorisation de diffusion des annotations et de cette fiche participant (supprimer les mentions inutiles):
- mes annotations ne doivent pas être gardées
- mes annotations peuvent être utilisées mais sans mon nom - CC-SA
- mes annotations peuvent être utilisées avec mon nom - CC-BY
- cette fiche participant ne doit pas être gardée
- cette fiche participant peut être utilisée mais sans mon nom - CC-SA
- cette fiche participant peut être utilisée avec mon nom - CC-BY


# Rappel des couches d'annotations
labelCouches_version complète : STDERM (si version incomplète : STDxRx)
S : phrase (segmentation et type selon le modèle GUM)
T : token (segmentation et POStagging selon le modèle des Universal Dependencies, lemme et normalisation orthographique si nécessaire)
D : dépendances syntaxique entre tokens selon le modèle des Universal Dependencies
E : entités nommées (segmentation des entitées nommées et typage selon le modèle Quaero)
R : (co)référence (segmentation des mentions selon le modèle DemoCrat, y compris les singletons, et relations de coréférence)
M : marqueurs discursifs (segmentation des marqueurs discursifs et typage selon le modèle de Crible & Degand (2019)



# Liste des annotations réalisées : 
Le tableau ci-dessous indique pour chaque texte manipulé le code des couches d'annotations (voir https://groupes.renater.fr/wiki/corligum) réalisées avec, si nécessaire, un commentaire
| ID texte | couche(s) | commentaire |
| ------ | ------ | ------ |
|        |        | ------ |
|        |        | ------ |

##Difficultés rencontrées


# Liste des adjudications réalisées : 
Le tableau ci-dessous indique pour chaque texte manipulé le code des couches d'annotations (https://groupes.renater.fr/wiki/corligum) réalisées avec, si nécessaire, un commentaire

| ID texte | couche(s) | commentaire |
| ------ | ------ | ------ |
|        |        | ------ |
|        |        | ------ |

##Difficultés rencontrées




# Journal de bord
Chaque action lié à au projet sera brièvement décrite et datée.
Exemple : 
- 01/12/2023 : segmentation en phrases du début du texte XXX  
...
