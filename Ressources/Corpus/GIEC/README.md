Voir le descriptif ici : **https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/Le-projet-CORLI-GUM/Corpus/corpus-GIEC**
* Le dossier "originaux" contient les fichiers d'origine du Corpus
* Le dossier 'annotations" contient les différentes versions annotées des fichiers
