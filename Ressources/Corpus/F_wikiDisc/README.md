Voir le descriptif ici : **https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/Le-projet-CORLI-GUM/Corpus/F_wikiDisc**
* Le dossier "originaux" contient les fichiers d'origine du Corpus
* Le dossier "convertis" contient les fichiers générés par le script https://corli.huma-num.fr/convinception qui permet de transformer des fichiers XML au format TEI en fichiers XML au format UIMA CAS (format demandé par INCEpTION) afin de conserver la trace des annotations TEI-P5 
* Le dossier 'annotations" contient les différentes versions annotées des fichiers
