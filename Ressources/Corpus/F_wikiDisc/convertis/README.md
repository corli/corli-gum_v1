Chaque archive est le résultat de la conversion par l'outil https://corli.huma-num.fr/convinception.
Ce corpus rassemble des pages de discussions Wikipedia. Lors de la conversion, chaque page a été segmentée en fils de discussion, ce qui a donné lieu à un archive contenant autant de fichier que de fil de discussion.
