La feuille de calcul permet de générer une matrice de confusion afin de comparer des annotations deux à deux.

Elle permet également de calculer le Kappa de Cohen.

Pour appliquer cette feuille de calcul aux annotation créées avec INCEpTION, il faut exporter le projet en indiquant une "Secondary format" : WebAnno TSV v3.3 (WebAnno v3.x) qui génére des fichiers tsv par annotateur pour chaque fichier texte.