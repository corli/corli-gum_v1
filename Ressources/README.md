Un dossier par corpus comprenant :
- Les fichiers d'origine du corpus
- Les fichiers annotés
- Les couches d'annotations et tagsets à importer dans INCEpTION
- Description & publications

Les informations, publications associées aux corpus sont disponibles sur le [Wiki](https://gitlab.huma-num.fr/corli/corli-gum_v1/-/wikis/Le-projet-CORLI-GUM/Corpus)
