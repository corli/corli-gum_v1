[[_TOC_]]

## CORLI GUM

- Présentation générale du projet CORLI GUM [sur le Wiki](https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/CORLI-GUM)
- [CORLI GUM pour les chercheureuses](https://gitlab.huma-num.fr/mguernut/corli-gum_v1/-/wikis/Le-projet-CORLI-GUM#les-chercheureuses-dans-corli-gum)

Dossier Espaces : 
- Espace CORLI-GUM
- Espace exploratoire
